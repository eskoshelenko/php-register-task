<?php

/**
 * Class LoggerService
 */
class LoggerService
{
  /**
   * @var resource|false
   */
  private $handle;
  /**
   * @var string
   */
  private $separator = '|';

  /**
   * @var LoggerService
   */
  protected static $instance;

  /**
   * LoggerService constructor
   * @return void
   */
  protected function __construct()
  {
    $fileName = '/store/' . date('Y-m-d') . '.log';

    $this->handle = fopen($_SERVER['DOCUMENT_ROOT'] . $fileName, 'a+');
  }

  /**
   * LoggerService clone
   * @return void
   */
  protected function __clone()
  {}

  /**
   * Creates LoggerService instance if it doesn't exists
   * @return LoggerService
   */
  public static function getInstance()
  {
    if (self::$instance === null) {
        self::$instance = new self;
    }

    return self::$instance;
  }

	/**
	 * Save logs into file
	 * @param string $level
	 * @param string $message
	 */
	public static function writeLog(string $level, string $message): void {
    $instance = static::getInstance();
    $message = implode($instance->separator, [
      date('G:i:s'), 
      $level, 
      print_r($message, true) . "\r\n"
    ]);

    fwrite($instance->handle, $message);
	}
}