<?php

/**
 * Class ValidationService
 */
class ValidationService
{
  /**
   * @var integer
   */
  private int $statusCode = 400;
  /**
   * @var array
   */
  private array $messages = [];
  /**
   * @var array
   */
  private array $postData;

  /**
   * ValidationService constructor
   * @param array $postData
   * @return void
   */
  public function __construct(array $postData)
  {
    $this->postData = $postData;
  }
  
  /**
   * Check email
   * @param string $key
   * @return ValidationService
   */
  public function isEmail(string $key): ValidationService  
  {
    $emailRegEx = '/^\w+[@]\w+[.]\w+$/';
    $isEmail = (bool) preg_match($emailRegEx, $this->postData[$key]);

    if (!$isEmail) {
      $this->messages[] = 'Provided ' . $key . ' is not email';
    }

    return $this;
  }

  /**
   * Check is email exists
   * @param string $email
   * @param string $key
   * @return ValidationService
   */
  public function isExists(
    array $users, 
    string $key
  ): ValidationService
  {
    $isExists = false;

    foreach ($users as $user) {
      if ($user[$key] === $this->postData[$key]) {
        $isExists = true;
        break;
      }
    }

    if ($isExists) {
      $this->messages[] = 'This ' . $key . ' already exists';
    }

    return $this;
  }

  /**
   * Check is passwords matches
   * @param string $keyOne
   * @param string $keyTwo
   * @return ValidationService
   */
  public function isMatches(
    string $keyOne, 
    string $keyTwo
  ): ValidationService
  {
    $isMatch = $this->postData[$keyOne] === $this->postData[$keyTwo];

    if (!$isMatch) {
      $message = 'Fields ' . $keyOne . ', ' . $keyTwo;
      $message .= ' don\'t matches';

      $this->messages[] = $message;
    }

    return $this;
  }

  /**
   * Check is passwords matches
   * @param integer $successCode
   * @param string $message
   * @return ValidationService
   */
  public function isValid(int $successCode, string $message): array
  {
    if (empty($this->messages)) {
      return [
        'statusCode' => $successCode,
        'message'    => $message
      ];
    }

    return [
      'statusCode' => $this->statusCode,
      'message'    => implode(', ', $this->messages)
    ];
  }
}