<?php
include '../../services/LoggerService.php';
include '../../services/ValidationService.php';

$db =  json_decode(file_get_contents('../accets/db.json'), true);

if ($_POST) {
  $validateService = new ValidationService($_POST);

  $result = $validateService
    ->isEmail('email')
    ->isExists($db['users'], 'email')
    ->isMatches('password', 'repeat')
    ->isValid(201, 'Registration success!');

  $statusCode = $result['statusCode'];
  $level = $statusCode < 400 ? 'info' : 'error';
  $message = $result['message'];

  http_response_code($statusCode);
  LoggerService::writeLog($level, $_POST['email'] . '|' . $message);

  echo $message;
}
