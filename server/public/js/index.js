"use strict";
function serialize(collection) {
    let serialize = '';
    for (const { name, value } of collection) {
        if (name)
            serialize += `${name}=${value}&`;
    }
    return serialize.slice(0, -1);
}
function alertUser(type, message) {
    return `
    <div class="alert alert-${type}" role="alert">
      ${message}
    </div>
  `;
}
function submitHandler(e) {
    const form = e.target;
    const { elements, action, method, enctype, previousElementSibling } = form;
    const options = {
        method,
        credentials: 'same-origin',
        headers: {
            'Content-Type': enctype || 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        body: serialize(elements)
    };
    e.preventDefault();
    fetch(action, options)
        .then(res => Promise.all([Promise.resolve(res.status), res.text()]))
        .then(([status, data]) => {
        let type = 'danger';
        if (status < 400) {
            form.setAttribute('hidden', 'true');
            type = 'success';
            Array.from(elements).forEach(element => {
                element.value = '';
            });
        }
        previousElementSibling.innerHTML = alertUser(type, data);
    })
        .catch(console.log);
}
document.body.addEventListener('submit', submitHandler, true);
//# sourceMappingURL=index.js.map